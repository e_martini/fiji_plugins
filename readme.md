# COLLECTION of plugins for Fiji/ImageJ Image Analysis software

collection of fiji plugins and macro in different languages for different purposes

### Prerequisites

1. [Fiji](http://fiji.sc/) updated

### Installing

1. Update your Fiji

### Organisation
Inside the source folder there are subfolders with plugins
## Authors

* **Emanuele Martini** - *Initial work* -  


## License

Ask to the author
