# ClawJ

Plugin to calculate the curvature angle of claws with the Feduccia's Method (1993, DOI: 10.1126/science.259.5096.790).


## Getting Started

Open an Image,
run the ClawJ plugin from the Plugin menu.
Put the point by hand in the order requested.


### Prerequisites

1. [Fiji](http://fiji.sc/) updated


### Installing

1. Update your Fiji
2. Close Fiji
3. Copy ClawJ_.py in the Fiji Plugin Folder
4. Restart Fiji

## Tutorial
Check the video and the image on the repository to understand how to calculate the curvature


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Emanuele Martini** - *Initial work* -


## License

Ask to the author
