from ij import IJ, WindowManager
from ij.plugin.frame import RoiManager
from ij.measure import ResultsTable
from ij.gui import WaitForUserDialog, Line, PointRoi
from java.awt import Color
import math
from ij.text import TextWindow


def draw_lines_by_points(P1, P2, roim, imp, line_name, line_color):
    x1 = (P1.XBase);
    x2 = (P2.XBase);
    y1 = (P1.YBase);
    y2 = (P2.YBase);
    # elongate lines
    m = (y1 - y2) / (x1 - x2);
    q = -(y1 - y2) / (x1 - x2) * x1 + y1;

        
    line = Line(x1, y1, x2, y2);
    if (line_color != None):
        line.setStrokeColor(line_color)
        line.setName(line_name)
        roim.addRoi(line);
        imp.setRoi(line);
    return m, q;    
    


imp = WindowManager.getCurrentImage();
roim = RoiManager().getInstance()
if roim.getCount()>0:
    WindowManager.getWindow("ROI Manager").close()
    
roim = RoiManager().getInstance()

# DRAW POINTS 

roim.runCommand("UseNames", "true");
points_name = ["A", "B", "X"];
points_color = Color.RED;

IJ.setTool("point");
# add points and set proper names
for p in range(0, 3):
    WaitForUserDialog("DRAW POINTS", "DRAW - " + points_name[p] + " - point to start analysis then click OK").show()             
    roi_point = imp.getRoi();
    roi_point.setName(str(points_name[p]));
    roi_point.setStrokeColor(points_color);
    roim.addRoi(roi_point);

# get Points
p_A = roim.getRoi(0);
p_B = roim.getRoi(1);
p_X = roim.getRoi(2);

roim.runCommand(imp, "Show All with labels");
WaitForUserDialog("ADJUST POINTS", "Adjust points to draw lines then click OK").show()   

# draw AB,AX,BX
m_AB, q_AB = draw_lines_by_points(p_A, p_B, roim, imp, "AB", Color.GREEN);
m_AX, q_AX = draw_lines_by_points(p_A, p_X, roim, imp, "AX", Color.GREEN);
m_BX, q_BX = draw_lines_by_points(p_B, p_X, roim, imp, "BX", Color.GREEN);


img_width = imp.getWidth();
img_height = imp.getHeight();

# get Center of inner circle by 3 points
x_center = (m_AX*m_BX*(-p_A.YBase+p_B.YBase)+m_AX*(p_X.XBase+p_B.XBase)-m_BX*(p_X.XBase+p_A.XBase))/(2*(m_AX-m_BX));
y_center = -1/m_AX*(x_center-(p_X.XBase+p_A.XBase)/2)+(p_X.YBase+p_A.YBase)/2;
P_center = PointRoi(int(x_center), int(y_center));
P_center.setName("E");
roim.addRoi(P_center);

#draw P_center - X
m_XC, q_XC = draw_lines_by_points(p_X, P_center, roim, imp, "X-Center", Color.YELLOW);

# get Y (Angle E_1-A ^ E_1-B)
m_E_1_A, q_E_1_A = draw_lines_by_points(p_A, P_center, roim, imp, "E_1-A", Color.ORANGE);
m_E_2_A, q_E_2_A = draw_lines_by_points(p_B, P_center, roim, imp, "E_1-B", Color.ORANGE);

tan_Y = ((m_E_1_A-m_E_2_A)/(1+m_E_2_A*m_E_1_A));
Y = math.atan(tan_Y);
## check and correct
print(Y*180/math.pi)
if ((Y*180/math.pi)<180) and (Y>0):	
	Y = -(Y-math.pi)
	print('correction : '+ str(Y*180/math.pi))
if (Y<0):
	Y = -Y
	print('correction : '+ str(Y*180/math.pi))

# update Results table
rt_exist = WindowManager.getWindow("Results_Claw_Curvature");
if rt_exist==None or not isinstance(rt_exist, TextWindow): 
    rt= ResultsTable();
else:
    rt = rt_exist.getTextPanel().getOrCreateResultsTable();



rt.incrementCounter();
rt.addValue("Image", imp.getTitle());
rt.addValue("Angle", Y*180/math.pi);
rt.show("Results_Claw_Curvature");

# line check perpendicular and bisecting
# E_AX perpendicular to AX pass by P_Circle
m_E_AX = 1 / m_AX;
x_p1 = P_center.XBase;
y_p1 = P_center.YBase;
y_p2 = (10.1);
x_p2 = (-y_p2 + m_E_AX * x_p1 + y_p1) / m_E_AX;
p2_R2 = PointRoi(int(x_p2), int(y_p2))
m2, q2 = draw_lines_by_points(P_center, p2_R2, roim, imp, "E_AX", Color.BLUE);

# E_BX perpendicular to AX pass by P_Circle
m_E_BX = 1 / m_BX;
x_p1 = P_center.XBase;
y_p1 = P_center.YBase;
y_p2 = (10.1);
x_p2 = (-y_p2 + m_E_BX * x_p1 + y_p1) / m_E_BX;
p2_R2 = PointRoi(int(x_p2), int(y_p2))
m2, q2 = draw_lines_by_points(P_center, p2_R2, roim, imp, "E_BX", Color.BLUE);


roim.runCommand(imp, "Show All with labels");